package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.paging.PageResponse;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueOperations;
import com.atlassian.psmq.api.queue.QueueQuery;
import com.google.common.collect.Lists;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.psmq.api.property.QPropertySetBuilder.newSet;
import static com.atlassian.psmq.api.property.query.QPropertyQueryBuilder.named;
import static com.atlassian.psmq.api.queue.QueueQueryBuilder.newQuery;
import static com.atlassian.psmq.backdoor.internal.tests.Fixtures.createQ;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 */
public class TestApiQueueQueries extends BaseApiTest {
    private QueueOperations queueOps;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        queueOps = qSession.queueOperations();
    }

    @Test
    public void test_basic_query() {
        // assemble
        QTopic topic = QTopic.newTopic("topic");
        String qName1 = Fixtures.rqName();
        String qName2 = Fixtures.rqName();
        String qName3 = Fixtures.rqName();

        Queue queue1 = createQ(qSession, some(qName1), Option.none());
        Queue queue2 = createQ(qSession, some(qName2), some(topic));
        Queue queue3 = createQ(qSession, some(qName3), some(topic));

        // act
        QueueQuery byId = newQuery().withId(queue1.id()).build();
        PageResponse<Queue> streamById = qSession.queueOperations().queryQueues(byId);

        QueueQuery byIdAndWrongName = newQuery().withId(queue1.id()).withName(qName2).build();
        PageResponse<Queue> streamByIdWrongName = queueOps.queryQueues(byIdAndWrongName);

        QueueQuery byTopic = newQuery().withTopic(topic).build();
        PageResponse<Queue> streamByTopic = queueOps.queryQueues(byTopic);

        QueueQuery byTopicAndName = newQuery().withName(qName3).withTopic(topic).build();
        PageResponse<Queue> streamByTopicAndName = queueOps.queryQueues(byTopicAndName);

        // assert
        assertOnlyQueue(streamById, queue1);

        assertThat(streamByIdWrongName.size(), equalTo(0));

        assertThat(streamByTopic.size(), equalTo(2));

        assertOnlyQueue(streamByTopicAndName, queue3);
    }

    @Test
    public void test_query_by_properties_long_values() throws Exception {
        Queue queue1 = createQ(qSession, some(Fixtures.rqName()), newSet().with("weight", 666).with("height", 999).build());
        Queue queue2 = createQ(qSession, some(Fixtures.rqName()), newSet().with("weight", 1666).with("height", 1999).build());
        Queue queue3 = createQ(qSession, some(Fixtures.rqName()), newSet().with("weight", 2666).with("height", 2999).build());

        // act
        PageResponse<Queue> byWeight1 = queueOps.queryQueues(newQuery().withProperties(named("weight").eq(666)).build());
        PageResponse<Queue> byWeight2 = queueOps.queryQueues(newQuery().withProperties(named("weight").gt(666)).build());


        // assert
        assertOnlyQueue(byWeight1, queue1);
        assertOnlyQueue(byWeight2, queue2, queue3);
    }

    @Test
    public void test_query_by_properties_string_values() throws Exception {
        Queue queue1 = createQ(qSession, some(Fixtures.rqName()), newSet().with("weight", "666kg").with("height", "999cm").build());
        Queue queue2 = createQ(qSession, some(Fixtures.rqName()), newSet().with("weight", "1666kg").with("height", "2999cm").build());
        Queue queue3 = createQ(qSession, some(Fixtures.rqName()), newSet().with("weight", "2666pounds").with("height", "1999cm").build());

        // act
        PageResponse<Queue> byWeightLong = queueOps.queryQueues(newQuery().withProperties(named("weight").eq(666)).build());
        PageResponse<Queue> byWeightStr = queueOps.queryQueues(newQuery().withProperties(named("weight").eq("666kg")).build());
        PageResponse<Queue> byWeightContains = queueOps.queryQueues(newQuery().withProperties(named("weight").contains("66")).build());
        PageResponse<Queue> byWeightStartsWith = queueOps.queryQueues(newQuery().withProperties(named("weight").startsWith("166")).build());
        PageResponse<Queue> byWeightEndsWith = queueOps.queryQueues(newQuery().withProperties(named("weight").endsWith("pounds")).build());


        // assert
        assertThat(byWeightLong.size(), equalTo(0));

        assertOnlyQueue(byWeightStr, queue1);
        assertOnlyQueue(byWeightContains, queue1, queue2, queue3);
        assertOnlyQueue(byWeightStartsWith, queue2);
        assertOnlyQueue(byWeightEndsWith, queue3);
        assertOnlyQueue(byWeightEndsWith, queue3);
    }

    private void assertOnlyQueue(final PageResponse<Queue> page, final Queue... expected) {
        List<String> expectedQueues;
        expectedQueues = Lists.transform(Arrays.asList(expected), q -> q.name());

        List<String> list = new ArrayList<>();
        page.forEach(q -> list.add(q.name()));

        assertThat(list.size(), equalTo(expectedQueues.size()));
        for (String expectedQueue : expectedQueues) {
            assertThat(list, CoreMatchers.hasItem(expectedQueue));
        }
    }
}
