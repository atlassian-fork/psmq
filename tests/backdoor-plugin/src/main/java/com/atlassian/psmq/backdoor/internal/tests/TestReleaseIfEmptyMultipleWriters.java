package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.psmq.api.QSession;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.atlassian.psmq.api.queue.QueueDefinitionBuilder;
import com.atlassian.util.concurrent.ThreadFactories;
import org.junit.Test;

import java.util.Optional;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestReleaseIfEmptyMultipleWriters extends BaseApiTest {

    private static final QueueDefinition QUEUE_DEFINITION = QueueDefinitionBuilder.newDefinition()
            .withName("release-if-empty-test-queue")
            .build();

    private static final int NUM_WRITERS = 3;
    private static final int NUM_READERS = 5;
    private static final int MESSAGE_COUNT_PER_WRITER = 100;

    private void say(String msg) {
        long l = TimeUnit.MICROSECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS);
        System.out.println(String.format("%d %s - %s", l, Thread.currentThread().getName(), msg));
    }

    @Test
    public void releaseIfEmpty__test_contention_with_multiple_writers() throws Exception {
        CountDownLatch writersStartedLatch = new CountDownLatch(NUM_WRITERS);
        CountDownLatch readersFinishedLatch = new CountDownLatch(NUM_READERS);

        AtomicInteger finished = new AtomicInteger(0);
        AtomicInteger messageCount = new AtomicInteger(0);
        CyclicBarrier barrier = new CyclicBarrier(NUM_READERS);

        ExecutorService writeExecutorService = Executors.newFixedThreadPool(
                NUM_WRITERS,
                ThreadFactories.namedThreadFactory("writer-thread")
        );
        for (int i = 0; i < NUM_WRITERS; i++) {
            writeExecutorService.submit(new MessageWriter(finished, writersStartedLatch));
            messageCount.addAndGet(MESSAGE_COUNT_PER_WRITER);
        }

        ExecutorService readExecutorService = Executors.newFixedThreadPool(
                NUM_READERS,
                ThreadFactories.namedThreadFactory("reader-thread")
        );
        for (int i = 0; i < NUM_READERS; i++) {
            readExecutorService.submit(new MessageReader(
                    messageCount,
                    finished,
                    barrier,
                    readersFinishedLatch,
                    writersStartedLatch
            ));
        }

        readersFinishedLatch.await();
        say("Finished reading messages");
        assertThat(messageCount.get(), equalTo(0));
    }


    private class MessageReader implements Runnable {

        private final AtomicInteger messageCount;
        private final AtomicInteger finished;
        private final CyclicBarrier barrier;
        private final CountDownLatch readersFinishedLatch;
        private final CountDownLatch writersStartedLatch;

        private final QSession session = psmqFixture.autoCommitSession();

        private MessageReader(
                AtomicInteger messageCount,
                AtomicInteger finished,
                CyclicBarrier barrier,
                CountDownLatch readersFinishedLatch,
                CountDownLatch writersStartedLatch) {
            this.messageCount = messageCount;
            this.finished = finished;
            this.barrier = barrier;
            this.readersFinishedLatch = readersFinishedLatch;
            this.writersStartedLatch = writersStartedLatch;
        }

        @Override
        public void run() {
            try {
                doRun();
            } catch (InterruptedException | BrokenBarrierException e) {
                say("Reader interrupted");
            } finally {
                readersFinishedLatch.countDown();
            }
        }

        private void doRun() throws InterruptedException, BrokenBarrierException {
            writersStartedLatch.await();

            say("Reader entering message read loop");
            while (true) {
                boolean writersFinished = finished.get() == NUM_WRITERS;
                Optional<Queue> queueOpt = session.queueOperations().exclusiveAccessQueue(QUEUE_DEFINITION);

                if (!queueOpt.isPresent()) {
                    barrier.await();
                } else {
                    processAllMessages(queueOpt.get());
                }

                if (writersFinished) {
                    say("Writers have finished, giving up");
                    return;
                }

                Thread.sleep(500);
            }
        }

        private void processAllMessages(Queue queue) throws InterruptedException, BrokenBarrierException {
            QMessageConsumer consumer = session.createConsumer(queue);

            say("Reading messages");
            do {
                Optional<QMessage> msg = consumer.claimAndResolve();
                if (msg.isPresent()) {
                    say(msg.get().buffer().asString());
                    messageCount.decrementAndGet();
                }
            } while (!session.queueOperations().releaseQueueIfEmpty(queue));

            say("Queue became empty, giving up exclusive access");
            barrier.await();
            barrier.reset();
        }

    }


    private class MessageWriter implements Runnable {

        private final AtomicInteger finished;
        private final CountDownLatch writersStartedLatch;

        private final QSession session = psmqFixture.autoCommitSession();

        private MessageWriter(
                AtomicInteger finished,
                CountDownLatch writersStartedLatch) {
            this.finished = finished;
            this.writersStartedLatch = writersStartedLatch;
        }

        @Override
        public void run() {
            try {
                doRun();
            } catch (Exception e) {
                e.printStackTrace();
                say("Writer interrupted");
            } finally {
                say("Writer finished");
                finished.incrementAndGet();
            }
        }

        private void doRun() throws InterruptedException {
            say("Writer started");
            writersStartedLatch.countDown();

            Queue queue = session.queueOperations().accessQueue(QUEUE_DEFINITION);
            QMessageProducer producer = session.createProducer(queue);

            for (int i = 0; i < MESSAGE_COUNT_PER_WRITER; i++) {
                QMessage msg = createMessage(i);
                producer.writeMessage(msg);

                Thread.sleep(randomTimeout());
            }
        }

        private QMessage createMessage(int value) {
            String threadName = Thread.currentThread().getName();
            return Fixtures.buildMsg(threadName + ": " + value).build();
        }

        private long randomTimeout() {
            int min = 10;
            int max = 100;

            double rand = Math.random();
            return min + (long) (rand * (max - min));
        }

    }

}
