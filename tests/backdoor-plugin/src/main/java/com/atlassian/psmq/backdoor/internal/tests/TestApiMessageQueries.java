package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.message.QContentType;
import com.atlassian.psmq.api.message.QContentVersion;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageId;
import com.atlassian.psmq.api.message.QMessagePriority;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.message.QMessageQuery;
import com.atlassian.psmq.api.message.QMessageQueryBuilder;
import com.atlassian.psmq.api.property.query.QPropertyQueryBuilder;
import com.atlassian.psmq.api.queue.QBrowseMessageQuery;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueBrowser;
import org.junit.Test;

import java.util.Optional;

import static com.atlassian.psmq.api.property.QPropertySetBuilder.newSet;
import static com.atlassian.psmq.api.queue.QBrowseMessageQueryBuilder.newQuery;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 */
public class TestApiMessageQueries extends BaseApiTest {
    @Test
    public void test_message_query_by_priority() throws Exception {
        Queue queue = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.none());

        QMessageProducer producer = qSession.createProducer(queue);

        QMessage high1 = Fixtures.buildMsg("high1").withPriority(QMessagePriority.HIGH).build();
        QMessage high2 = Fixtures.buildMsg("high2").withPriority(QMessagePriority.HIGH).build();
        QMessage low1 = Fixtures.buildMsg("low1").withPriority(QMessagePriority.LOW).build();

        producer.writeMessage(high1);
        producer.writeMessage(high2);
        producer.writeMessage(low1);

        // act
        QMessageConsumer consumer = qSession.createConsumer(queue);

        QMessageQuery messageQuery = QMessageQueryBuilder.newQuery().withPriority(QMessagePriority.HIGH).build();
        Optional<QMessage> msg1 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg2 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg3 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg4 = consumer.claimAndResolve(QMessageQueryBuilder.anyMessage());

        // assert
        assertThat(msg1.isPresent(), equalTo(true));
        assertThat(msg1.map(m -> m.buffer().asString()).get(), equalTo("high1"));

        assertThat(msg2.isPresent(), equalTo(true));
        assertThat(msg2.map(m -> m.buffer().asString()).get(), equalTo("high2"));

        assertThat(msg3.isPresent(), equalTo(false));

        assertThat(msg4.isPresent(), equalTo(true));
        assertThat(msg4.map(m -> m.buffer().asString()).get(), equalTo("low1"));
    }

    @Test
    public void test_message_query_by_content_type() throws Exception {
        Queue queue = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.none());

        QMessageProducer producer = qSession.createProducer(queue);

        QMessage json1 = Fixtures.buildMsg("js1").withContentType(QContentType.JSON).build();
        QMessage json2 = Fixtures.buildMsg("js2").withContentType(QContentType.JSON).build();
        QMessage text1 = Fixtures.buildMsg("text").withContentType(QContentType.PLAIN_TEXT).build();

        producer.writeMessage(json1);
        producer.writeMessage(json2);
        producer.writeMessage(text1);

        // act
        QMessageConsumer consumer = qSession.createConsumer(queue);

        QMessageQuery messageQuery = QMessageQueryBuilder.newQuery().withContentType(QContentType.JSON).build();
        Optional<QMessage> msg1 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg2 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg3 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg4 = consumer.claimAndResolve(QMessageQueryBuilder.anyMessage());

        // assert
        assertThat(msg1.isPresent(), equalTo(true));
        assertThat(msg1.map(m -> m.buffer().asString()).get(), equalTo("js1"));

        assertThat(msg2.isPresent(), equalTo(true));
        assertThat(msg2.map(m -> m.buffer().asString()).get(), equalTo("js2"));

        assertThat(msg3.isPresent(), equalTo(false));

        assertThat(msg4.isPresent(), equalTo(true));
        assertThat(msg4.map(m -> m.buffer().asString()).get(), equalTo("text"));
    }


    @Test
    public void test_message_query_by_content_version() throws Exception {
        Queue queue = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.none());

        QMessageProducer producer = qSession.createProducer(queue);

        QMessage in1 = Fixtures.buildMsg("v1").withContentType(QContentType.JSON).withContentVersion(QContentVersion.V1).build();
        QMessage in2 = Fixtures.buildMsg("v2").withContentType(QContentType.JSON).withContentVersion(QContentVersion.V2).build();
        QMessage in3 = Fixtures.buildMsg("v2").withContentType(QContentType.JSON).withContentVersion(QContentVersion.V2).build();

        producer.writeMessage(in1);
        producer.writeMessage(in2);
        producer.writeMessage(in3);

        // act
        QMessageConsumer consumer = qSession.createConsumer(queue);

        QMessageQuery messageQuery = QMessageQueryBuilder.newQuery().withContentVersion(QContentVersion.V2).build();
        Optional<QMessage> msg1 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg2 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg3 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg4 = consumer.claimAndResolve(QMessageQueryBuilder.anyMessage());

        // assert
        assertThat(msg1.isPresent(), equalTo(true));
        assertThat(msg1.map(m -> m.buffer().asString()).get(), equalTo("v2"));

        assertThat(msg2.isPresent(), equalTo(true));
        assertThat(msg2.map(m -> m.buffer().asString()).get(), equalTo("v2"));

        assertThat(msg3.isPresent(), equalTo(false));

        assertThat(msg4.isPresent(), equalTo(true));
        assertThat(msg4.map(m -> m.buffer().asString()).get(), equalTo("v1"));
    }

    @Test
    public void test_message_query_by_message_id() throws Exception {
        Queue queue = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.none());

        QMessageProducer producer = qSession.createProducer(queue);

        QMessage in1 = Fixtures.buildMsg("m1").withId(QMessageId.id("id1")).build();
        QMessage in2 = Fixtures.buildMsg("m2").withId(QMessageId.id("id2")).build();
        QMessage in3 = Fixtures.buildMsg("m3").withId(QMessageId.id("id1")).build();

        producer.writeMessage(in1);
        producer.writeMessage(in2);
        producer.writeMessage(in3);

        // act
        QMessageConsumer consumer = qSession.createConsumer(queue);

        QMessageQuery messageQuery = QMessageQueryBuilder.newQuery().withMessageId(QMessageId.id("id1")).build();
        Optional<QMessage> msg1 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg2 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg3 = consumer.claimAndResolve(messageQuery);

        // assert
        assertThat(msg1.isPresent(), equalTo(true));
        assertThat(msg1.map(m -> m.buffer().asString()).get(), equalTo("m1"));


        assertThat(msg2.isPresent(), equalTo(true));
        assertThat(msg2.map(m -> m.buffer().asString()).get(), equalTo("m3"));

        assertThat(msg3.isPresent(), equalTo(false));
    }

    @Test
    public void test_message_query_combination() throws Exception {
        Queue queue = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.none());

        QMessageProducer producer = qSession.createProducer(queue);

        QMessage in1 = Fixtures.buildMsg("m1").withId(QMessageId.id("id1")).withContentType(QContentType.JSON).withContentVersion(QContentVersion.V0).build();
        QMessage in2 = Fixtures.buildMsg("m2").withId(QMessageId.id("id1")).withContentType(QContentType.JSON).withContentVersion(QContentVersion.V1).build();
        QMessage in3 = Fixtures.buildMsg("m3").withId(QMessageId.id("id1")).withContentType(QContentType.JSON).withContentVersion(QContentVersion.V2).build();
        QMessage in4 = Fixtures.buildMsg("m4").withId(QMessageId.id("id1")).withContentType(QContentType.JSON).withContentVersion(QContentVersion.V3).build();

        producer.writeMessage(in1);
        producer.writeMessage(in2);
        producer.writeMessage(in3);
        producer.writeMessage(in4);

        // act
        QMessageConsumer consumer = qSession.createConsumer(queue);

        QMessageQuery messageQuery = QMessageQueryBuilder.newQuery().withMessageId(QMessageId.id("id1")).withContentType(QContentType.JSON).withContentVersion(QContentVersion.V2).build();

        Optional<QMessage> msg1 = consumer.claimAndResolve(messageQuery);

        messageQuery = QMessageQueryBuilder.newQuery().withMessageId(QMessageId.id("id1")).withContentType(QContentType.JSON).build();

        Optional<QMessage> msg2 = consumer.claimAndResolve(messageQuery);
        Optional<QMessage> msg3 = consumer.claimAndResolve(messageQuery);


        messageQuery = QMessageQueryBuilder.newQuery().withMessageId(QMessageId.id("id1")).withContentType(QContentType.PLAIN_TEXT).build();

        Optional<QMessage> msg4 = consumer.claimAndResolve(messageQuery);

        messageQuery = QMessageQueryBuilder.newQuery().withContentType(QContentType.JSON).build();

        Optional<QMessage> msg5 = consumer.claimAndResolve(messageQuery);


        // assert
        assertThat(msg1.isPresent(), equalTo(true));
        assertThat(msg1.map(m -> m.buffer().asString()).get(), equalTo("m3"));

        assertThat(msg2.isPresent(), equalTo(true));
        assertThat(msg2.map(m -> m.buffer().asString()).get(), equalTo("m1"));

        assertThat(msg3.isPresent(), equalTo(true));
        assertThat(msg3.map(m -> m.buffer().asString()).get(), equalTo("m2"));

        assertThat(msg4.isPresent(), equalTo(false));

        assertThat(msg5.isPresent(), equalTo(true));
        assertThat(msg5.map(m -> m.buffer().asString()).get(), equalTo("m4"));

    }


    @Test
    public void test_message_browse_count_by_query() throws Exception {
        Queue queue = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.none());

        QMessageProducer producer = qSession.createProducer(queue);

        QMessage in1 = Fixtures.buildMsg("m1").withId(QMessageId.id("id1")).withContentType(QContentType.JSON).withContentVersion(QContentVersion.V0).withProperties(newSet().with("weight", "666kg").with("height", "999cm").build()).build();
        QMessage in2 = Fixtures.buildMsg("m2").withId(QMessageId.id("id1")).withContentType(QContentType.JSON).withContentVersion(QContentVersion.V1).withProperties(newSet().with("weight", "999kg").with("height", "999cm").build()).build();
        QMessage in3 = Fixtures.buildMsg("m3").withId(QMessageId.id("id1")).withContentType(QContentType.JSON).withContentVersion(QContentVersion.V2).withProperties(newSet().with("weight", "1999pounds").with("height", "1999inches").build()).build();
        QMessage in4 = Fixtures.buildMsg("m4").withId(QMessageId.id("id1")).withContentType(QContentType.JSON).withContentVersion(QContentVersion.V3).build();

        producer.writeMessage(in1);
        producer.writeMessage(in2);
        producer.writeMessage(in3);
        producer.writeMessage(in4);

        // act
        QBrowseMessageQuery byWeight = newQuery().withProperties(QPropertyQueryBuilder.named("weight").eq("666kg")).build();

        QBrowseMessageQuery byWeightInKilos = newQuery().withProperties(QPropertyQueryBuilder.named("weight").contains("kg")).build();

        QueueBrowser browser = qSession.queueOperations().browser(queue);
        long byWeightCount = browser.getMessageCount(byWeight);
        long byWeightInKilosCount = browser.getMessageCount(byWeightInKilos);


        // assert
        assertThat(byWeightCount, equalTo(1L));
        assertThat(byWeightInKilosCount, equalTo(2L));

    }
}