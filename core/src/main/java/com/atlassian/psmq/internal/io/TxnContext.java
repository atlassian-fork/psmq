package com.atlassian.psmq.internal.io;

import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.psmq.api.message.QClaimant;

/**
 * A TxnContext is passed into code that is taking part in a Txn
 */
public interface TxnContext
{
    /**
     * @return the connection to use for this Txn work
     */
    DatabaseConnection connection();

    /**
     * @return the claimant info in force for this session
     */
    QClaimant claimant();

    /**
     * @return the claimant heart beat millis
     */
    long claimantHeartBeatMillis();
}
