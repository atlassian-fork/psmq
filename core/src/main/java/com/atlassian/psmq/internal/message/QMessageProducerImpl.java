package com.atlassian.psmq.internal.message;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.message.QMessageUpdate;
import com.atlassian.psmq.internal.QCloseableImpl;
import com.atlassian.psmq.internal.io.MessageWriter;
import com.atlassian.psmq.internal.io.SessionInstructions;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static java.lang.String.format;

/**
 */
public class QMessageProducerImpl extends QCloseableImpl implements QMessageProducer
{
    private final MessageWriter messageWriter;
    private final SessionInstructions instructions;

    public QMessageProducerImpl(final MessageWriter messageWriter, final SessionInstructions instructions)
    {
        this.instructions = instructions;
        this.messageWriter = messageWriter;
    }

    @Override
    public long writeMessage(final QMessage msg) throws QException
    {
        checkNotNull(msg);
        checkNotClosed(format("This message producer is closed : '%s'", name()));
        return messageWriter.writeMessage(instructions, msg);
    }

    @Override
    public void updateMessage(QMessage msg, QMessageUpdate messageUpdate) throws QException {
        checkNotNull(msg);
        checkNotNull(messageUpdate);
        checkNotClosed(format("This message producer is closed : '%s'", name()));
        messageWriter.updateMessage(instructions, msg, messageUpdate);
    }

    private String name()
    {
        Option<String> topic = instructions.topic().map(t -> t.value());
        Option<String> qName = instructions.queue().map(q -> q.name());
        return qName.getOrElse(topic.getOrElse("???"));
    }
}
