package com.atlassian.psmq.spi;

/**
 * The actual database tables in PSMQ can be externally provided. An OSGI implementation of this interface will be searched for to get the
 * table names to be used to store messages in.  This allows the storage of the PSMQ tables to be provided in a implementation specific
 * way.  The shape of the tables themselves is very specific and cant be easily made dynamic.  They must match the internal shapes required
 * by the code. But the names and location of the tables can vary if need be.
 */
public interface QDatabaseTableProvider {

    /**
     * Returns the name of the table that provides the specific function.
     *
     * @param table the desired table
     * @return the actual name of that table
     */
    String getTableName(LogicalTableName table);

    enum LogicalTableName {
        QUEUE, MESSAGE, MESSAGE_PROPERTY, QUEUE_PROPERTY
    }
}
