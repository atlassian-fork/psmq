package com.atlassian.psmq.api.queue;

import com.atlassian.psmq.api.paging.DefaultPagingLimits;
import com.atlassian.psmq.api.paging.LimitedPageRequest;
import com.atlassian.psmq.api.paging.LimitedPageRequestImpl;
import com.atlassian.psmq.api.paging.PageRequest;
import com.atlassian.psmq.api.paging.SimplePageRequest;
import com.atlassian.psmq.api.property.query.QPropertyQuery;

import java.util.Optional;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;

/**
 * A builder of {@link com.atlassian.psmq.api.queue.QueueQuery}s
 *
 * @since v1.0
 */
public class QueueQueryBuilder {

    Optional<QId> queueId = Optional.empty();
    Optional<String> queueName = Optional.empty();
    Optional<QTopic> topic = Optional.empty();
    Optional<QPropertyQuery> properties = Optional.empty();
    PageRequest paging = new SimplePageRequest(0, DefaultPagingLimits.DEFAULT_QUEUES_RETURNED);

    /**
     * @return a builder of queue queries
     */
    public static QueueQueryBuilder newQuery() {
        return new QueueQueryBuilder();
    }

    public QueueQueryBuilder withId(QId queueId) {
        this.queueId = Optional.of(queueId);
        return this;
    }

    public QueueQueryBuilder withName(String queueName) {
        this.queueName = Optional.of(queueName);
        return this;
    }

    public QueueQueryBuilder withTopic(QTopic topic) {
        this.topic = Optional.of(topic);
        return this;
    }

    public QueueQueryBuilder withProperties(QPropertyQuery propertyQuery) {
        this.properties = Optional.of(propertyQuery);
        return this;
    }

    public QueueQueryBuilder withPaging(PageRequest paging) {
        this.paging = checkNotNull(paging);
        return this;
    }

    public QueueQueryBuilder withPaging(int start, int limit) {
        this.paging = new SimplePageRequest(start, limit);
        return this;
    }


    public QueueQuery build() {
        return new QueueQuery() {
            @Override
            public Optional<QId> queueId() {
                return queueId;
            }

            @Override
            public Optional<String> queueName() {
                return queueName;
            }

            @Override
            public Optional<QTopic> topic() {
                return topic;
            }

            @Override
            public Optional<QPropertyQuery> properties() {
                return properties;
            }

            @Override
            public LimitedPageRequest paging() {
                return LimitedPageRequestImpl.create(paging, DefaultPagingLimits.MAX_QUEUES_RETURNED);
            }
        };
    }
}
