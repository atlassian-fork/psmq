package com.atlassian.psmq.api.queue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.property.QPropertySet;

import java.util.Date;
import java.util.Optional;

/**
 * A Queue is where you place {@link com.atlassian.psmq.api.message.QMessage}s into.
 *
 * @since v0.x
 */
@PublicApi
public interface Queue
{
    /**
     * @return the id of the queue
     */
    QId id();

    /**
     * @return the name of the queue
     */
    String name();

    /**
     * @return the purpose of the queue
     */
    String purpose();

    /**
     * @return the topic associated with this queue
     */
    Optional<QTopic> topic();

    /**
     * @return the date the queue was created
     */
    Date created();
    /**
     * @return the date the queue was last modified
     */
    Date lastModified();

    /**
     * @return an approximate message count on how many messages are in that queue
     */
    long approxMsgCount();

    /**
     * @return the properties associated with this message
     */
    QPropertySet properties();
}
