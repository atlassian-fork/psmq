package com.atlassian.psmq.api.queue;

import com.atlassian.annotations.PublicApi;

/**
 * This represents an unique identifier that is assigned to a queue.
 *
 * @since v1.0
 */
@PublicApi
public class QId {
    private final long value;

    public QId(final long value) {
        this.value = value;
    }

    /**
     * Returns a queue id with the specified value
     *
     * @param id the id to use
     * @return a QId
     */
    public static QId id(long id) {
        return new QId(id);
    }

    /**
     * @return the value of the queue id
     */
    public long value() {
        return value;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final QId that = (QId) o;

        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Long.valueOf(value).hashCode();
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
