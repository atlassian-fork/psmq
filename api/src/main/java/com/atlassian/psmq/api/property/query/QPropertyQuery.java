package com.atlassian.psmq.api.property.query;

import com.atlassian.annotations.PublicApi;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Property queries are predicates of named properties and their values and an operator
 *
 * @since 1.0
 */
@PublicApi
public class QPropertyQuery extends PropertyExpressions.BasePropertyExpression<QPropertyQuery>
{
    private final Operator operator;
    private final List<PropertyExpressions.PropertyExpression> expressions;

    QPropertyQuery(final Operator operator, final PropertyExpressions.PropertyExpression left)
    {
        super(null);
        this.operator = operator;
        this.expressions = ImmutableList.of(left);
    }

    QPropertyQuery(final Operator operator, final PropertyExpressions.PropertyExpression left, PropertyExpressions.PropertyExpression right)
    {
        super(null);
        this.operator = operator;
        this.expressions = ImmutableList.of(left, right);
    }

    public Operator getOperator()
    {
        return operator;
    }

    public PropertyExpressions.PropertyExpression getLeft()
    {
        return expressions.get(0);
    }

    public PropertyExpressions.PropertyExpression getRight()
    {
        return expressions.get(1);
    }

    public QPropertyQuery and(QPropertyQuery expression)
    {
        return new QPropertyQuery(Operator.AND, this, expression);
    }

    public QPropertyQuery or(QPropertyQuery expression)
    {
        return new QPropertyQuery(Operator.OR, this, expression);
    }

    @Override
    public <R, C> R accept(final PropertyExpressions.Visitor<R, C> visitor, final C context)
    {
        return null;
    }

    @Override
    public String toString()
    {
        Objects.ToStringHelper helper = Objects.toStringHelper(this);
        if (expressions.size() > 0)
        {
            helper.add("lhs", expressions.get(0));
        }
        helper.add("operator", operator);
        if (expressions.size() > 1)
        {
            helper.add("rhs", expressions.get(1));
        }
        return helper.toString();
    }


    /**
     * The canonical list of operations
     */
    public enum Operator
    {
        // logical
        AND, OR,

        // equality
        EQ, NE, IS_NOT_NULL, IS_NULL,

        // compare
        GOE, GT, LOE, LT,

        // string
        EQ_IGNORE_CASE, CONTAINS, STARTS_WITH, ENDS_WITH, LIKE
    }
}
