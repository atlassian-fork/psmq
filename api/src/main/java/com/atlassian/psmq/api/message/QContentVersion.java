package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;

/**
 * {@link com.atlassian.psmq.api.message.QMessage}s can have a content version value to help you interpret them when reading them
 *
 * @since v1.0
 */
@PublicApi
public class QContentVersion
{
    /**
     * Unspecified (-1) content version
     */
    public static final QContentVersion UNSPECIFIED = new QContentVersion(-1);
    /**
     * Pre-canned version 0
     */
    public static final QContentVersion V0 = new QContentVersion(0);
    /**
     * Pre-canned version 1
     */
    public static final QContentVersion V1 = new QContentVersion(1);
    /**
     * Pre-canned version 2
     */
    public static final QContentVersion V2 = new QContentVersion(2);
    /**
     * Pre-canned version 3
     */
    public static final QContentVersion V3 = new QContentVersion(3);

    private final int value;

    public static QContentVersion version(int value)
    {
        return new QContentVersion(value);
    }

    public QContentVersion(final int value)
    {
        this.value = value;
    }

    /**
     * @return the content version value
     */
    public int value()
    {
        return value;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final QContentVersion that = (QContentVersion) o;

        return value == that.value;

    }

    @Override
    public int hashCode()
    {
        return value;
    }
}
