package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.property.QProperty;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

/**
 * The definition of a message update.  You can build this via {@link QMessageUpdateBuilder}
 */
@PublicApi
public interface QMessageUpdate {
    /**
     * @return (optionally) the date the message will expire
     */
    Optional<Date> expiryDate();

    /**
     * @return (optionally) the priority of the message
     */
    Optional<QMessagePriority> priority();

    /**
     * @return the properties to be associated with this message
     */
    Optional<Set<QProperty>> properties();

}
