package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.property.query.QPropertyQuery;

import java.util.Optional;

/**
 * A builder of {@link com.atlassian.psmq.api.message.QMessageQuery}s
 *
 * @since v1.0
 */
@PublicApi
public class QMessageQueryBuilder {
    Optional<QContentType> contentType = Optional.empty();
    Optional<QContentVersion> version = Optional.empty();
    Optional<QMessagePriority> priority = Optional.empty();
    Optional<QMessageId> messageId = Optional.empty();
    Optional<QPropertyQuery> properties = Optional.empty();

    /**
     * @return a MessageQuery that finds any message, that is without criteria
     */
    public static QMessageQuery anyMessage() {
        return newQuery().build();
    }

    public static QMessageQueryBuilder newQuery() {
        return new QMessageQueryBuilder();
    }

    /**
     * This will find messages using the given content type as criteria
     *
     * @param contentType the content type in play
     *
     * @return the builder
     */
    public QMessageQueryBuilder withContentType(QContentType contentType) {
        this.contentType = Optional.of(contentType);
        return this;
    }

    /**
     * This will find messages using the given content version as criteria
     *
     * @param version the content version in play
     *
     * @return the builder
     */
    public QMessageQueryBuilder withContentVersion(QContentVersion version) {
        this.version = Optional.of(version);
        return this;
    }

    /**
     * This will find messages using the given message id as criteria
     *
     * @param messageId the message id in play
     *
     * @return the builder
     */
    public QMessageQueryBuilder withMessageId(QMessageId messageId) {
        this.messageId = Optional.of(messageId);
        return this;
    }

    /**
     * This will find messages using the given message priority as criteria
     *
     * @param priority the message priority in play
     *
     * @return the builder
     */
    public QMessageQueryBuilder withPriority(QMessagePriority priority) {
        this.priority = Optional.of(priority);
        return this;
    }

    /**
     * This will find messages using the given property query as criteria
     *
     * @param propertyQuery the property query in play
     *
     * @return the builder
     */
    public QMessageQueryBuilder withProperties(QPropertyQuery propertyQuery) {
        this.properties = Optional.of(propertyQuery);
        return this;
    }


    public QMessageQuery build() {
        return new QMessageQuery() {
            @Override
            public Optional<QContentType> contentType() {
                return contentType;
            }

            @Override
            public Optional<QContentVersion> contentVersion() {
                return version;
            }

            @Override
            public Optional<QMessagePriority> priority() {
                return priority;
            }

            @Override
            public Optional<QMessageId> messageId() {
                return messageId;
            }

            @Override
            public Optional<QPropertyQuery> properties() {
                return properties;
            }
        };
    }
}
