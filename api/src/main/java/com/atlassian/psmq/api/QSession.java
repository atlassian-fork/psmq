package com.atlassian.psmq.api;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.message.QCloseable;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueOperations;

/**
 * A QSession represents a connected and stateful queueing session to the system.
 *
 * You need to be careful about the lifecycle of this session since handles to the database can be in place.
 *
 * The session is created with specific "behaviour" in place around when messages are considered committed on the queue
 */
@PublicApi
public interface QSession extends QCloseable
{
    /**
     * @return the queue operations you can perform
     */
    QueueOperations queueOperations();

    /**
     * A consumer that can read messages from the specified queue
     *
     * @param queue the queue to read messages from
     * @return a session linked message consumer
     * @throws QException in exception circumstances
     */
    QMessageConsumer createConsumer(Queue queue) throws QException;

    /**
     * A consumer that can write messages to the specified queue
     *
     * @param queue the queue to write messages to
     * @return a session linked message producer
     * @throws QException in exception circumstances
     */
    QMessageProducer createProducer(Queue queue) throws QException;

    /**
     * A consumer that can write messages to the specified topic.  It will be placed in all queues associated with this
     * topic.
     *
     * @param topic the topic to write messages toward
     * @return a session linked message producer
     * @throws QException in exception circumstances
     */
    QMessageProducer createProducer(QTopic topic) throws QException;

    /**
     * Called to commit the message reads and writes
     *
     * You must ONLY call this if the session is {@link com.atlassian.psmq.api.QSessionDefinition.CommitStrategy#SESSION_COMMIT}
     *
     * @throws QException in exception circumstances
     */
    void commit() throws QException;

    /**
     * Called to rollback the message reads and writes
     *
     * You must ONLY call this if the session is {@link com.atlassian.psmq.api.QSessionDefinition.CommitStrategy#SESSION_COMMIT}
     *
     * @throws QException in exception circumstances
     */
    void rollback() throws QException;
}
