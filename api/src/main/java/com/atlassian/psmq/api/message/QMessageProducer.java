package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.QException;

/**
 * A producer of {@link com.atlassian.psmq.api.message.QMessage}s that writes them to a {@link com.atlassian.psmq.api.queue.Queue}
 *
 * @since v1.0
 */
@PublicApi
public interface QMessageProducer  extends QCloseable
{
    /**
     * Writes the given message to the underlying {@link com.atlassian.psmq.api.queue.Queue}
     *
     * @param msg the message to write
     * @return the number of objects written to by this operation
     *
     * @throws QException in exceptional circumstances
     */
    long writeMessage(QMessage msg) throws QException;

    /**
     * This can be called to update a message that resides in a queue.
     *
     * @param msg           the message to update
     * @param messageUpdate the update to be performed.  You can build this via {@link QMessageUpdateBuilder}
     * @throws QException in exceptional circumstances
     */
    void updateMessage(QMessage msg, QMessageUpdate messageUpdate) throws QException;
}
