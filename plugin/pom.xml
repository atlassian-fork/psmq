<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>atlassian-psmq-parent</artifactId>
        <groupId>com.atlassian.psmq</groupId>
        <version>1.0.1-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>psmq-plugin</artifactId>
    <name>psmq-plugin</name>
    <description>PSMQ - Provides Pretty Simple Message Queueing</description>
    <packaging>atlassian-plugin</packaging>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.psmq</groupId>
            <artifactId>psmq-api</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>com.atlassian.psmq</groupId>
            <artifactId>psmq-core</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>com.atlassian.psmq</groupId>
            <artifactId>psmq-common</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-core</artifactId>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-annotation</artifactId>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-runtime</artifactId>
        </dependency>

        <dependency>
            <groupId>com.atlassian.pocketknife</groupId>
            <artifactId>atlassian-pocketknife-querydsl</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.activeobjects</groupId>
            <artifactId>activeobjects-plugin</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.fugue</groupId>
            <artifactId>fugue</artifactId>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
        </dependency>

    </dependencies>


    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.labs</groupId>
                <artifactId>maven-versionator</artifactId>
                <version>1.2</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>versionate</goal>
                        </goals>
                        <phase>validate</phase>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>com.atlassian.plugin</groupId>
                <artifactId>atlassian-spring-scanner-maven-plugin</artifactId>
                <version>${atlassian.spring.scanner.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>atlassian-spring-scanner</goal>
                        </goals>
                        <phase>prepare-package</phase>
                        <!-- process-classes seems to be skipped for scala -->
                    </execution>
                </executions>
                <configuration>
                    <!-- Turn this on to true to show the inner workings of the build time scanner -->
                    <verbose>true</verbose>
                    <scannedDependencies>
                        <dependency>
                            <groupId>com.atlassian.psmq</groupId>
                            <artifactId>psmq-core</artifactId>
                        </dependency>
                        <dependency>
                            <groupId>com.atlassian.pocketknife</groupId>
                            <artifactId>atlassian-pocketknife-querydsl</artifactId>
                        </dependency>
                    </scannedDependencies>

                </configuration>
            </plugin>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-jira-plugin</artifactId>
                <extensions>true</extensions>
                <configuration>
                    <productVersion>${jira.version}</productVersion>
                    <productDataVersion>${jira.data.version}</productDataVersion>

                    <enableFastdev>false</enableFastdev>
                    <useFastdevCli>false</useFastdevCli>
                    <enableDevToolbox>false</enableDevToolbox>
                    <enablePde>false</enablePde>

                    <skipRestDocGeneration>true</skipRestDocGeneration>
                    <allowGoogleTracking>false</allowGoogleTracking>
                    <skipManifestValidation>true</skipManifestValidation>
                    <extractDependencies>false</extractDependencies>
                    <skipManifestValidation>true</skipManifestValidation>

                    <!-- install database drivers - they go into WEB-INF/lib -->
                    <libArtifacts>
                        <libArtifact>
                            <groupId>org.postgresql</groupId>
                            <artifactId>postgresql</artifactId>
                            <version>${postgres.jdbc.version}</version>
                        </libArtifact>
                        <libArtifact>
                            <groupId>mysql</groupId>
                            <artifactId>mysql-connector-java</artifactId>
                            <version>5.1.25</version>
                        </libArtifact>
                        <libArtifact>
                            <groupId>com.oracle</groupId>
                            <artifactId>oracle-jdbc15</artifactId>
                            <version>11.2.0.1.0</version>
                        </libArtifact>
                        <libArtifact>
                            <groupId>net.sourceforge.jtds</groupId>
                            <artifactId>jtds</artifactId>
                            <version>1.2.4</version>
                        </libArtifact>
                    </libArtifacts>

                    <!--These are put there via AMPs -->
                    <pluginArtifacts>
                        <pluginArtifact>
                            <groupId>com.atlassian.labs.plugins</groupId>
                            <artifactId>quickreload</artifactId>
                            <version>${quick.reload.version}</version>
                        </pluginArtifact>
                        <pluginArtifact>
                            <groupId>com.atlassian.psmq</groupId>
                            <artifactId>psmq-backdoor-plugin</artifactId>
                            <version>${project.version}</version>
                        </pluginArtifact>
                        <pluginArtifact>
                            <groupId>com.atlassian.psmq</groupId>
                            <artifactId>psmq-rest-plugin</artifactId>
                            <version>${project.version}</version>
                        </pluginArtifact>
                    </pluginArtifacts>

                    <instructions>
                        <Atlassian-Plugin-Key>com.atlassian.psmq</Atlassian-Plugin-Key>
                        <Bundle-SymbolicName>com.atlassian.psmq</Bundle-SymbolicName>
                        <Spring-Context>*</Spring-Context>

                        <Export-Package>
                            com.atlassian.psmq.api;version=${project.version.threePartVersion},
                            com.atlassian.psmq.api.*;version=${project.version.threePartVersion},
                            com.atlassian.psmq.spi;version=${project.version.threePartVersion},
                            com.atlassian.psmq.spi.*;version=${project.version.threePartVersion},

                            <!--Testing only -->
                            com.atlassian.psmq.internal.visiblefortesting;version=${project.version.threePartVersion}

                        </Export-Package>

                        <Import-Package>
                            <!-- Platform 4 use Gemini while the old uses SpringDM -->
                            org.springframework.*;resolution:="optional",
                            org.eclipse.gemini.blueprint.*;resolution:="optional",

                            <!-- reflection based -->
                            org.apache.log4j;resolution:="optional",

                            javax.annotation,

                            com.atlassian.psmq.api,
                            com.atlassian.psmq.api.*,
                            com.atlassian.psmq.spi,
                            com.atlassian.psmq.spi.*,

                            <!-- let bind discover the rest -->
                            *;resolution:=optional
                        </Import-Package>
                    </instructions>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
