package com.atlassian.psmq.internal.util.rest;

import com.google.common.collect.Iterables;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.stream.JsonWriter;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Objects;
import java.util.function.Consumer;
import javax.ws.rs.core.StreamingOutput;

/**
 * Captures a JsonWriter and a Gson serializer and no longer throws IOExceptions
 */
public class JsonOut implements Closeable
{
    private final Gson gson;
    private final JsonWriter writer;

    public JsonOut(OutputStream outputStream) throws UnsupportedEncodingException
    {
        gson = new Gson();
        writer = new JsonWriter(new OutputStreamWriter(outputStream, "UTF-8"));
        writer.setIndent("  ");
    }

    public Gson gson()
    {
        return gson;
    }

    public JsonWriter writer()
    {
        return writer;
    }

    public JsonWriter beginArray()
    {
        return rte(effect -> writer.beginArray());
    }

    public JsonWriter endArray()
    {
        return rte(effect -> writer.endArray());
    }

    public JsonWriter inArray(StreamingRunnable runnable)
    {
        return rte(effect -> {
            writer.beginArray();
            runnable.run();
            writer.endArray();
        });
    }

    public JsonWriter arrayNamed(String name, StreamingRunnable runnable)
    {
        return rte(effect -> {
            writer.name(name);
            writer.beginArray();
            runnable.run();
            writer.endArray();
        });
    }

    public JsonWriter beginObject()
    {
        return rte(effect -> writer.beginObject());
    }

    public JsonWriter endObject()
    {
        return rte(effect -> writer.endObject());
    }

    public JsonWriter propertyNamed(String name, StreamingRunnable runnable)
    {
        return rte(effect -> {
            writer.name(name);
            writer.beginObject();
            runnable.run();
            writer.endObject();
        });
    }

    public JsonWriter propertyNamed(String name, Object value)
    {
        toJson(name,value);
        return writer;
    }

    public JsonWriter inObject(StreamingRunnable runnable)
    {
        return rte(effect -> {
            writer.beginObject();
            runnable.run();
            writer.endObject();
        });
    }

    public <T> JsonWriter inObjects(Iterable<T> iterable, Consumer<? super T> action)
    {
        return rte(effect -> {

            Objects.requireNonNull(action);
            for (T t : iterable) {
                writer.beginObject();
                action.accept(t);
                writer.endObject();
            }
        });
    }

    public JsonWriter value(final boolean value)
    {
        return rte(effect -> writer.value(value));
    }

    public JsonWriter value(final double value)
    {
        return rte(effect -> writer.value(value));
    }

    public JsonWriter value(final long value)
    {
        return rte(effect -> writer.value(value));
    }

    public JsonWriter value(final Number value)
    {
        return rte(effect -> writer.value(value));
    }

    public JsonWriter value(final String value)
    {
        return rte(effect -> writer.value(value));
    }

    public JsonWriter name(final String name)
    {
        return rte(effect -> writer.name(name));
    }

    public JsonWriter nullValue()
    {
        return rte(effect -> writer.nullValue());
    }

    public void flush()
    {
        rte(effect -> writer.flush());
    }

    public void close()
    {
        rte(effect -> writer.close());
    }

    public <T> void toJson(T data, Class<T> dataClass)
    {
        gson.toJson(data, dataClass, writer);
    }

    public <T> void toJson(T data)
    {
        Class<?> dataClass = data.getClass();
        gson.toJson(data, dataClass, writer);
    }

    public <T> void toJson(String name, T data)
    {
        rte(() -> writer.name(name));

        Class<?> dataClass = data.getClass();
        gson.toJson(data, dataClass, writer);
    }

    public static interface StreamingEffect<T>
    {
        public void apply(T effect) throws IOException;
    }

    public static interface StreamingRunnable
    {
        public void run() throws IOException;
    }

    private JsonWriter rte(StreamingEffect<JsonWriter> effect)
    {
        try
        {
            effect.apply(this.writer);
        }
        catch (IOException e)
        {
            throw new JsonIOException(e);
        }
        return this.writer;
    }

    private JsonWriter rte(StreamingRunnable runnable)
    {
        try
        {
            runnable.run();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        return this.writer;
    }

    /**
     * Starts the outputting of JSON with a top level object started before the effect is applied
     *
     * @param jsonOut the code to run
     * @return a streamy output
     */
    public static StreamingOutput run(StreamingEffect<JsonOut> jsonOut)
    {
        return output -> {
            JsonOut out = new JsonOut(output);
            try
            {
                out.beginObject();
                jsonOut.apply(out);
                out.endObject();
                out.close();
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        };
    }
}
